import 'package:flutter/material.dart';

class TextModel extends ChangeNotifier {
  List<Product> _products = [];
  List<Product> _products2 = [];
  double _total = 0;

  List<Product> get allProducts => _products;

  void add(Product product) {
    _products2.add(product);
    _total += product.total;
    notifyListeners();
  }

  void delete(Product product) {
    _total -= product.total;
    _products2.remove(product);
    notifyListeners();
  }

  double get total {
    return _total;
  }

  List<Product> get shoppingCard {
    return _products2;
  }

  void addProduct(Product product) {
    _products.add(product);
    notifyListeners();
  }

  void deleteProduct(Product product) {
    _products.remove(product);
    notifyListeners();
  }


  void detailProduct(Product product){

  }

  int get count {
    return _products.length;
  }

  int get countadd {
    return _products2.length;
  }
}

class Product {
  String productName;
  int jumlah;
  int harga;
  double total;
  String deskripsi;
  String img;
  bool isCompleted;
  Product({
    this.isCompleted,
    this.harga,
    this.jumlah,
    this.productName,
    this.total,
    this.img,
    this.deskripsi
  });
}
