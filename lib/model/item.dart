class Item {
  int id;
  String name;
  int harga;
  String deskripsi;
  String img;

  Item({
    this.id,
    this.name,
    this.harga,
    this.deskripsi,
    this.img,
  });

}
