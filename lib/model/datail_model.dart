import 'package:flutter/material.dart';
import 'package:provider_example/model/text_add_model.dart';

class DetailProduct extends ChangeNotifier {
  Product _products ;

  Product get getProduct => _products;
  void view (Product product){
    _products = product;
    notifyListeners();
  }

}