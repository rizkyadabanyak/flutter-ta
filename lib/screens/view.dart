import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_example/data.dart';
import 'package:provider_example/model/datail_model.dart';
import 'package:provider_example/model/item.dart';
import 'package:provider_example/model/text_add_model.dart';
import 'package:provider_example/screens/detail_view.dart';
import 'package:provider_example/screens/shopping_card_page.dart';

class ViewPage extends StatefulWidget {
  ViewPage({Key key}) : super(key: key);

  @override
  _ViewPageState createState() => _ViewPageState();
}

class _ViewPageState extends State<ViewPage> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController textController = TextEditingController();
  final TextEditingController priceController = TextEditingController();

  final List<Item> lisItem = items;


  Item dropdownValue ;

  @override
  Widget build(BuildContext context) {

    TextModel products = Provider.of<TextModel>(context);
    DetailProduct detail = Provider.of<DetailProduct>(context);

    return Consumer<TextModel>(builder: (context, textmodel, child) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: Center(child: Text('Kasir Sederhana :D')),
          actions: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ShoppingCardPage()));
                    },
                  ),
                  Text(textmodel.countadd.toString())
                ],
              ),
            )
          ],
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                    itemCount: products.allProducts.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: InkWell(
                          onTap: () {
                            detail.view(
                              Product(
                                productName: products.allProducts[index].productName,
                                harga : products.allProducts[index].harga,
                                isCompleted : products.allProducts[index].isCompleted,
                                jumlah : products.allProducts[index].jumlah,
                                deskripsi : products.allProducts[index].deskripsi,
                                img : products.allProducts[index].img,
                                total: products.allProducts[index].total
                                ),
                              );
                            Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => DetailItemPage()));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Image.asset(products.allProducts[index].img,width: 100),
                              ),
                              Text(products.allProducts[index].productName),
                              Text(
                                  'Rp.${products.allProducts[index].harga.toString()}'),
                              Text(
                                  'x ${products.allProducts[index].jumlah.toString()}'),
                              Row(
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.shopping_cart),
                                    onPressed: () {
                                      textmodel.add(products.allProducts[index]);
                                    },
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.delete),
                                    onPressed: () {
                                      products.deleteProduct(
                                          products.allProducts[index]);
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      );
                    }),
              ),
              Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.all(30),
                  child: Column(
                    children: [
                      DropdownButtonFormField(
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder( //<-- SEE HERE
                            // borderSide: BorderSide(color: Colors.black, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder( //<-- SEE HERE
                            // borderSide: BorderSide(color: Colors.black, width: 2),
                          ),
                          filled: true,
                          // fillColor: Colors.greenAccent,
                        ),
                        // dropdownColor: Colors.greenAccent,
                        value: dropdownValue,
                        onChanged: (Item newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        items: lisItem.map<DropdownMenuItem<Item>>((Item value) {
                          return DropdownMenuItem<Item>(
                            value: value,
                            child: Text(
                              value.name,
                              style: TextStyle(fontSize: 20),
                            ),
                          );
                        }).toList(),
                      ),
                      TextFormField(
                        controller: priceController,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Sending Message"),
                            ));
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                            hintText: "Jumlah",
                            suffixIcon: IconButton(
                              icon: Icon(Icons.clear),
                              onPressed: () {
                                priceController.clear();
                              },
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                products.addProduct(
                                  Product(
                                      jumlah: int.parse(priceController.text),
                                      productName: dropdownValue.name,
                                      harga: dropdownValue.harga,
                                      img: dropdownValue.img,
                                      deskripsi : dropdownValue.deskripsi,
                                    total: dropdownValue.harga * double.parse(priceController.text),
                                  ),
                                );
                              },
                              child: Text('Add'),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
