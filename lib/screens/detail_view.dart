import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_example/model/text_add_model.dart';

import '../model/datail_model.dart';


class DetailItemPage extends StatefulWidget {
  @override
  _DetailItemState createState() => _DetailItemState();
}

class _DetailItemState extends State<DetailItemPage> {
  @override
  Widget build(BuildContext context) {
    DetailProduct detail = Provider.of<DetailProduct>(context);

    return Consumer<TextModel>(
      builder: (context, textmodel, child) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.orange,
            title: Text("Detail Page ${detail.getProduct.productName}"),
          ),
          body: Container(
            child: Column(
              children: [
                Image.asset(detail.getProduct.img,width: 300),
                Text('${detail.getProduct.deskripsi}')
              ],
            ),
          ),
        );
      },
    );
  }
}
