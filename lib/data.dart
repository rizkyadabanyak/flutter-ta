
import 'package:provider_example/model/item.dart';

// final List<Item> doneTourismPlaceList = [];
final List<Item> items = [
  Item(
    id: 1,
    name: 'sampo',
    harga: 4000,
    deskripsi: 'ini description',
    img: 'assets/images/sampo.jpg',
  ),Item(
    id: 2,
    name: 'pepsoden',
    harga: 300000,
    deskripsi: 'ini description',
    img: 'assets/images/pepsoden.jpg',

  ),Item(
    id: 3,
    name: 'jagung',
    harga: 94000,
    deskripsi: 'ini description',
    img: 'assets/images/jagung.jpeg',

  ),Item(
    id: 4,
    name: 'kecap',
    harga: 831000,
    deskripsi: 'ini description',
    img: 'assets/images/kecap.jpg',
  ),
];